from pexpect import pxssh
import pexpect
import getpass
import time

def login(hostname='',auth=[],logpath="default_log.txt",login_timeout=6,etimeout=6):
        # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
        if len(auth) > 0:
            for au in auth:
                print ("Trying to Login:"+hostname)
                return_typ = None
                username = au.get("username")
                password = au.get("password")
                try:
                    s = pxssh.pxssh(options={
                                    "StrictHostKeyChecking": "no",
                                    "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
                    s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
                    s.logfile = open(logpath, "ab")
                    # Send enter to get router prompt to check login success
                    s.sendline('')
                    # expecting cisco , juniper , fortigate prompt
                    ex = ["#",">","\$",pexpect.TIMEOUT]
                    match_ex = s.expect(ex,timeout=etimeout)
                    login_chk = s.before.strip()
                    if len(login_chk) > 0 and match_ex < 3:
                        host_name = login_chk.decode("utf-8")
                        aftr = s.after
                        if type(aftr) == str:
                            host_name = host_name+aftr.strip().decode("utf-8")
                        print("Login Success :"+hostname+":"+host_name)
                        return s,host_name
                    else:
                        print("Not able to reach device:"+hostname)
                        return "TIMEOUT"
                except pxssh.ExceptionPxssh as e:
                    err = str(e)
                    if err.find("password refused") != -1:
                        print("Login Failed:"+hostname)
                        return_typ = "LOGINFAIL"
                    else:
                        print("Error>"+err+":"+hostname)
                        return "TIMEOUT"
                except Exception as e:
                    print("Unknown Error :"+str(e))
                    return "TIMEOUT"
            return return_typ


def nexus_memutil(s,monobj):
    try:
        out = {}
        exp = s[1]
	po=["Memory"]
        cmd = " sh system resources | i \"usage\" "
        s[0].sendline("terminal length 0")
        s[0].sendline(cmd)
        s[0].expect([cmd,pxssh.TIMEOUT],timeout=5)
        s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
        data = str(s[0].before)
        pos = data.find("Memory")
        if pos > -1:
             n = filter(None,data[pos:].split("\n")[0].split(" "))
             used = float(n[4].replace("K",""))
             total = float(n[2].replace("K",""))
             per = (used/total)*100
             per = int(round (per))
             out.update({"Memory":per})
             return out
    except Exception as e:
        print("cis_bgp Error 1>"+str(e))

auth = []
auth.append({"username":"admin","password":"admin@123"})
monobj = {"monitor":"raw","type":"raw","name":"raw"}
i = "192.168.106.19"
s = login(i,auth,"default_log.txt",6,6)
if type(s) == tuple:
    print (nexus_memutil (s,monobj))



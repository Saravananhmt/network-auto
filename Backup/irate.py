from pexpect import pxssh
import pexpect
import getpass
import time

def login(hostname='',auth=[],logpath="default_log.txt",login_timeout=6,etimeout=6):
        # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
        if len(auth) > 0:
            for au in auth:
                print ("Trying to Login:"+hostname)
                return_typ = None
                username = au.get("username")
                password = au.get("password")
                try:
                    s = pxssh.pxssh(options={
                                    "StrictHostKeyChecking": "no",
                                    "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
                    s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
                    s.logfile = open(logpath, "ab")
                    # Send enter to get router prompt to check login success
                    s.sendline('')
                    # expecting cisco , juniper , fortigate prompt
                    ex = ["#",">","\$",pexpect.TIMEOUT]
                    match_ex = s.expect(ex,timeout=etimeout)
                    login_chk = s.before.strip()
                    if len(login_chk) > 0 and match_ex < 3:
                        host_name = login_chk.decode("utf-8")
                        aftr = s.after
                        if type(aftr) == str:
                            host_name = host_name+aftr.strip().decode("utf-8")
                        print("Login Success :"+hostname+":"+host_name)
                        return s,host_name
                    else:
                        print("Not able to reach device:"+hostname)
                        return "TIMEOUT"
                except pxssh.ExceptionPxssh as e:
                    err = str(e)
                    if err.find("password refused") != -1:
                        print("Login Failed:"+hostname)
                        return_typ = "LOGINFAIL"
                    else:
                        print("Error>"+err+":"+hostname)
                        return "TIMEOUT"
                except Exception as e:
                    print("Unknown Error :"+str(e))
                    return "TIMEOUT"
            return return_typ


def cis_bgp(s,monobj):
    try:
        out = {}
        mon_ = monobj.get("monitor")
        mon_ = mon_.split(",")

        type_ = monobj.get("type")
        in_ = monobj.get("name")

        exp = s[1]
        cmd = "show int gi0/0 human | i rate "
        s[0].sendline("terminal length 0")
        s[0].sendline(cmd)
        s[0].expect([cmd,pxssh.TIMEOUT],timeout=5)
        s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
        data = str(s[0].before)
        print data
        for host in mon_:
            try:
                ot=""
                pos = data.find(host)
                print host
                if pos > -1:
                    n = filter(None,data[pos:].split("\n")[0].split(" "))
                    ot = "Speed:"+n[2]+n[3]
                    ot = ot.strip()
            except Exception as e:
                print("cis_bgp Error 2>"+str(e))
            out.update({host:ot})
        print out
        return out
    except Exception as e:
        print("cis_bgp Error 1>"+str(e))

auth = []
auth.append({"username":"sara","password":"Hmt@15"})
monobj = {"monitor":"input,output","type":"raw","name":"raw"}
i = "192.168.235.73"
s = login(i,auth,"default_log.txt",6,6)
if type(s) == tuple:
    print (cis_bgp (s,monobj))



from pexpect import pxssh
import pexpect
import getpass
import time

def login(hostname='',auth=[],logpath="default_log.txt",login_timeout=6,etimeout=6):
        # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
        if len(auth) > 0:
            for au in auth:
                print ("Trying to Login:"+hostname)
                return_typ = None
                username = au.get("username")
                password = au.get("password")
                try:
                    s = pxssh.pxssh(options={
                                    "StrictHostKeyChecking": "no",
                                    "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
                    s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
                    s.logfile = open(logpath, "ab")
                    # Send enter to get router prompt to check login success
                    s.sendline('')
                    # expecting cisco , juniper , fortigate prompt
                    ex = ["#",">","\$",pexpect.TIMEOUT]
                    match_ex = s.expect(ex,timeout=etimeout)
                    login_chk = s.before.strip()
                    if len(login_chk) > 0 and match_ex < 3:
                        host_name = login_chk.decode("utf-8")
                        aftr = s.after
                        if type(aftr) == str:
                            host_name = host_name+aftr.strip().decode("utf-8")
                        print("Login Success :"+hostname+":"+host_name)
                        return s,host_name
                    else:
                        print("Not able to reach device:"+hostname)
                        return "TIMEOUT"
                except pxssh.ExceptionPxssh as e:
                    err = str(e)
                    if err.find("password refused") != -1:
                        print("Login Failed:"+hostname)
                        return_typ = "LOGINFAIL"
                    else:
                        print("Error>"+err+":"+hostname)
                        return "TIMEOUT"
                except Exception as e:
                    print("Unknown Error :"+str(e))
                    return "TIMEOUT"
            return return_typ

def cis_checklist(s,monobj):
        mon_ = monobj.get("monitor")
        mon_ = mon_.split(",")
        s[0].sendline("terminal length 0")
        for cmd in mon_:
                 exp = s[1]
                 s[0].sendline(cmd)
                 s[0].expect([cmd,pxssh.TIMEOUT],timeout=5)
                 s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
                 data = str(s[0].before)
                 print cmd
                 print data


auth = []
auth.append({"username":"sara","password":"Hmt@09"})
monobj = {"monitor":"ping 192.168.196.120 so gi0/1.100 r 100,tel 192.168.196.120 22 /so gi0/1.100,quit,sh int g0/0 | i err,sh int gi0/0 | i Dup,sh int gi0/0 | i rate,sh clock","type":"raw","name":"raw"}
i = raw_input("Enter the IP to login:")
s = login(i,auth,"default_log.txt",6,6)
if type(s) == tuple:
        cis_checklist (s,monobj)


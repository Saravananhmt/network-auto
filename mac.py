from pexpect import pxssh
import pexpect
import getpass
import time

def login(hostname='',auth=[],logpath="mac_log.txt",login_timeout=6,etimeout=6):
        # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
        if len(auth) > 0:
            for au in auth:
                print ("Trying to Login:"+hostname)
                return_typ = None
                username = au.get("username")
                password = au.get("password")
                try:
                    s = pxssh.pxssh(options={
                                    "StrictHostKeyChecking": "no",
                                    "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
                    s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
                    s.logfile = open(logpath, "ab")
                    # Send enter to get router prompt to check login success
                    s.sendline('')
                    # expecting cisco , juniper , fortigate prompt
                    ex = ["#",">","\$",pexpect.TIMEOUT] 
                    match_ex = s.expect(ex,timeout=etimeout)
                    login_chk = s.before.strip()
                    if len(login_chk) > 0 and match_ex < 3:
                        host_name = login_chk.decode("utf-8")
                        aftr = s.after
                        if type(aftr) == str:
                            host_name = host_name+aftr.strip().decode("utf-8")
                        print("Login Success :"+hostname+":"+host_name)
                        return s,host_name
                    else:
                        print("Not able to reach device:"+hostname)
                        return "TIMEOUT"
                except pxssh.ExceptionPxssh as e:
                    err = str(e)
                    if err.find("password refused") != -1:
                        print("Login Failed:"+hostname)
                        return_typ = "LOGINFAIL"
                    else:
                        print("Error>"+err+":"+hostname)
                        return "TIMEOUT"
                except Exception as e:
                    print("Unknown Error :"+str(e))
                    return "TIMEOUT"
            return return_typ

def Add_Mac_Cisco(s):

        try :
          exp = s[1]
          s[0].sendline("config t")
          s[0].sendline("mac access-list extended NOC_USER")
          s[0].expect(["mac access-list extended NOC_USER",pxssh.TIMEOUT],timeout=5)
          s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
          data = str(s[0].before)
          print data
	  for m in Mac:
          	s[0].sendline("permit host  "+m+"  any")
          	#s[0].expect(["permit host  "+m+" any",pxssh.TIMEOUT],timeout=5)
          	s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
          	data = str(s[0].before)
		print data
          #s[0].sendline("do wr")
          print data
          return "success"
        except Exception as e:
           print e
           return "failed"

def Add_Mac_Juniper(s):

        try :
          exp = s[1]
          cmd = "set ethernet-switching-options secure-access-port interface MAC-bind allowed-mac  "
          s[0].sendline("edit")
	  for m in Mac:
          	s[0].sendline(cmd+m)
          #s[0].expect([cmd+Mac,pxssh.TIMEOUT],timeout=5)
          	s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
          	data = str(s[0].before)
          	print data
          s[0].sendline("commit")
          s[0].expect([exp,pxssh.TIMEOUT],timeout=5)
          data = str(s[0].before)
          print data
          return "success"
        except Exception as e:
           print e
           return "failed"

auth = []
US = str(raw_input("username:"))
PS = str(raw_input("password:"))
auth.append({"username":US,"password":PS})
Mac = str()
a = True
Mac = []
while a == True:
	m = str(raw_input("Enter the MAC:"))
	if len(m) > 10:
		Mac.append(m)
	else:
		a = False

cisco_hosts = ['192.168.176.188','192.168.176.185','192.168.176.186','192.168.176.189','192.168.176.179','192.168.233.142']
juniper_host = ['192.168.176.190','192.168.176.178']

for i in cisco_hosts:
        s = login(i,auth,"default_log.txt",6,6)
        if type(s) == tuple:
            print (Add_Mac_Cisco(s))

for i in juniper_host:
        s = login(i,auth,"default_log.txt",6,6)
        if type(s) == tuple:
            print (Add_Mac_Juniper(s))




